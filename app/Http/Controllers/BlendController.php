<?php

namespace App\Http\Controllers;

use App\Blend;
use App\UserFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BlendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('type')==='gallery')
        {
            return Blend::with(['files' => function($q) {
                $q->where('type','main_image');
            }])->get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()) {
            $request->validate([
                'title' => 'required|unique:blends,title|max:255',
                'main_image' =>'file|image',
                'extra_images.*.file' => 'file|image',
                'blend_file' => 'file',
                'gltf_file' => 'file'
            ]);
            if(!Blend::where('title',$request->input('title'))->count()){
                $user = Auth::user();
                $blend = new Blend();
                $blend->user_id = $user->id;
                $blend->title = $request->input('title');
                $imageDirectory = 'public/user_'.$user->id.'/blends/'.sha1($blend->title).'/images';
                $gltfDirectory = 'public/user_'.$user->id.'/blends/'.sha1($blend->title).'/gltf';
                Storage::makeDirectory($imageDirectory);
                Storage::makeDirectory($gltfDirectory);
                $blend->save();
                $mainImage =  new UserFile();
                $mainImage->file_location = $request->file('main_image')->store($imageDirectory);
                $mainImage->type = "main_image";
                $blend->files()->save($mainImage);
                foreach($request->file('extra_images') as $extraImageFile) {
                    $extraImage = new UserFile();
                    $extraImage->file_location = $extraImageFile->store($imageDirectory);
                    $extraImage->type = "extra_image";
                    $blend->files()->save($extraImage);
                }
                $gltfFile = $request->file('gltf_file');
                    $file = new UserFile();
                    $file->file_location = $gltfFile->storeAs($gltfDirectory, $gltfFile->getClientOriginalName());
                    $file->type = $gltfFile->extension();
                    $blend->files()->save($file);
                $blendFile =  new UserFile();
                $blendFile->file_location = $request->file('blend_file')->storeAs($imageDirectory,$request->file('blend_file')->getClientOriginalName());
                $blendFile->type = "blend";
                $blend->files()->save($blendFile);
                return ['message'=>[
                    'type'=>'success','content' => 'success'
                ]];
            } else {
                return ['message'=>[
                    'type'=>'error','content' => 'Blend with this title already exists'
                ]];
            }
        }
        else {
            return ['message'=>[
                'type'=>'error','content' => 'user not auth'
            ]];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blend  $blend
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return Blend::where('id',$id)
        ->with(['files','user'])
        ->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blend  $blend
     * @return \Illuminate\Http\Response
     */
    public function edit(Blend $blend)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blend  $blend
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blend $blend)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blend  $blend
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) 
    {   
        $user = Auth::User();
        $blend = Blend::find($id);
        $directory = 'public/user_'.$user->id.'/blends/'.$blend->title.'/';
        if($user) {
            if($user->id === $blend->user->id) {
                Storage::deleteDirectory($directory);
                foreach($blend->files as $file) {
                    $file->delete();
                }
            $blend->delete();
            return Auth::User();
            return 'supuestamente se elimino';
            }
            else {
                return "Auth user is not owner of this resource";
            }
        } else return  ['error','User not auth ot auth user is not the owner of this resourse'];
    }
}
