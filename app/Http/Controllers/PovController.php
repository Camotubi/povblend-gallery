<?php

namespace App\Http\Controllers;

use App\Pov;
use App\UserFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PovController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('type')==='gallery')
        {
            return Pov::with(['files' => function($q) {
                $q->where([
                    ['type','main_image'],
                    ['file_location','<>','']
                    ]);
            }])->get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()) {
            $request->validate([
                'title' => 'required|unique:povs,title|max:255',
                'main_image' =>'file|image',
                'extra_images.*.file' => 'file|image',
                'pov_file' => 'file',
            ]);
            if(!Pov::where('title',$request->input('title'))->count()){
                $user = Auth::user();
                $pov = new Pov();
                $pov->user_id = $user->id;
                $pov->title = $request->input('title');
                $imageDirectory = 'public/user_'.$user->id.'/povs/'.sha1($pov->title).'/images';
                Storage::makeDirectory($imageDirectory);
                $pov->save();
                $mainImage =  new UserFile();
                $mainImage->file_location = $request->file('main_image')->store($imageDirectory);
                $mainImage->type = "main_image";
                $pov->files()->save($mainImage);
                foreach($request->file('extra_images') as $extraImageFile) {
                    $extraImage = new UserFile();
                    $extraImage->file_location = $extraImageFile->store($imageDirectory);
                    $extraImage->type = "extra_image";
                    $pov->files()->save($extraImage);
                }
                $povFile =  new UserFile();
                $povFile->file_location = $request->file('pov_file')->storeAs($imageDirectory,$request->file('pov_file')->getClientOriginalName());
                $povFile->type = "pov";
                $pov->files()->save($povFile);
                return ['message'=>[
                    'type'=>'success','content' => 'success'
                ]];
            } else {
                return ['message'=>[
                    'type'=>'error','content' => 'Blend with this title already exists'
                ]];
            }
        }
        else {
            return ['message'=>[
                'type'=>'error','content' => 'user not auth'
            ]];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pov  $pov
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return Pov::where('id',$id)
        ->with(['files','user'])
        ->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pov  $pov
     * @return \Illuminate\Http\Response
     */
    public function edit(Pov $pov)
    {
        $user = Auth::User();
        $pov = Pov::find($id);
        $directory = 'public/user_'.$user->id.'/povs/'.$pov->title.'/';
        if($user) {
            if($user->id === $pov->user->id) {
                Storage::deleteDirectory($directory);
                foreach($pov->files as $file) {
                    $file->delete();
                }
            $pov->delete();
            return Auth::User();
            return 'supuestamente se elimino';
            }
            else {
                return "Auth user is not owner of this resource";
            }
        } else return  ['error','User not auth ot auth user is not the owner of this resourse'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pov  $pov
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pov $pov)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pov  $pov
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pov $pov)
    {
        //
    }
}
