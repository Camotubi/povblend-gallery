<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserFile extends Model
{
    //protected $table='user_files';
    public function fileable() {
        $this->morphTo();
    }

    public function getFileLocationAttribute($value) {
        return Storage::url($value);
    }
}
