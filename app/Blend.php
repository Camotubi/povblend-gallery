<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blend extends Model
{
    public function files() {
        return $this->morphMany(UserFile::class,'fileable');
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
}
