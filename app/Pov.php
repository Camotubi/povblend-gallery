<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pov extends Model
{
    public function files() {
        return $this->morphMany(UserFile::class,'fileable');
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
}
