import Vue from 'vue';
import Router from 'vue-router';
import UserIndex from '../components/User/UserIndex';
import UserShow from '../components/User/UserShow';
import BlenderShow from '../components/Blender/BlenderShow';
import BlenderCreate from '../components/Blender/BlenderCreate';
import BlenderIndex from '../components/Blender/BlenderIndex';
import PovCreate from '../components/Pov/PovCreate';
import PovIndex from '../components/Pov/PovIndex';
import PovShow from '../components/Pov/PovShow';
import Register from '../components/Register';
import Login from '../components/Login';

export default new Router({
  routes: [
    {
      path: '/users',
      name: 'UsersIndex',
      component: UserIndex,
    },
    {
      path:'/users/:id',
      name: 'UserShow',
      component: UserShow,
      props: true,
    },
      {
      path:'/povs',
      name: 'PovIndex',
      component: PovIndex,
    },
    {
      path:'/povs/create',
      name: 'PovCreate',
      component: PovCreate,
      meta: {
        auth:true
      }
    },
    {
      path:'/povs/:id',
      name: 'PovShow',
      component: PovShow,
      props:true,
      
    },
      {
      path:'/blender',
      name: 'BlenderIndex',
      component: BlenderIndex,
    },
    {
      path:'/blender/create',
      name: 'BlenderCreate',
      component: BlenderCreate,
      meta: {
        auth:true
      }
    },
    {
      path:'/blender/:id',
      name: 'BlenderShow',
      component: BlenderShow,
      props:true,
      
    },
    {
      path:'/register',
      name:'register',
      component:Register,
      meta: {
        auth:false
      }
      },
      {
        path:'/login',
        name:'login',
        component:Login,
        meta:{
          auth:false
        }
      }
    
  ]
})