
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import vue from 'vue';
import vuetify from 'vuetify';
import vueRouter from 'vue-router';
import axios from 'axios';
import vueAxios from 'vue-axios';
import router from './router/index';
import appVue from './components/App';
import 'vuetify/dist/vuetify.min.css';
 
vue.use(vueRouter);
vue.use(vuetify);
vue.use(vueAxios, axios);
vue.router = new vueRouter;
axios.defaults.baseURL = 'http://190.219.115.156:8080/api';
vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    authRedirect:{path:'/login'},
 });


 


const app = new vue({
    el: '#app',
    components: { appVue },
    render: h => h(appVue),
    router: router,
});

