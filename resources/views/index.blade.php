<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/app.css">
    </head>
    <body>
        <div id = "app" >
        </div>
    <script src="/js/app.js"></script>
    </body>
</html>
